# LobsterFTP

Author: Hugo Forraz
Language: Rust

February, the 12th 2023

---

## Table of Contents <!-- omit in toc -->

- [Work done](#work-done)
- [Introduction](#introduction)
  - [What is LobsterFTP?](#what-is-lobsterftp)
  - [How to use it?](#how-to-use-it)
- [Architecture](#architecture)
- [Code Samples](#code-samples)


---

## Work done

- [X] CMP    	The code compiles correctly using ~~Maven~~ **Cargo** 
- [X] DOC    	The code is documented (README.md, ~~Javadoc~~ **Rustdoc**) 
- [X] TST   	The code is tested (unit tests ~~using JUnit~~) 
- [X] COO   	The code is made using ~~OOC principles~~ Rust's good practices 
- [X] EXE   	The code can be executed
- [X] CON   	I can connect to the server using a FTP Client
- [X] BADU   	The server rejects my connection if I try to connect as an unknown user
- [X] BADP   	The server rejects my connection if I give a wrong password
- [X] LST   	I can list the content of a distant directory using the LIST command
- [X] CWD   	I can change the current directory using the CWD command
- [X] CDUP   	I can change the current directory to the parent directory using the CDUP command
- [X] ROOT   	I can't go above the root directory for the current user
- [X] GETT   	I can download a text file
- [X] GETB   	I can download a binary file (image)
- [X] GETR   	I can download a whole directory
- [X] PUTT   	I can upload a text file
- [X] PUTB   	I can upload a binary file (image)
- [X] PUTR   	I can upload a whole directory
- [X] RENF   	I can rename a distant file
- [X] MKD   	I can create a new directory
- [X] REND   	I can rename a distant directory
- [X] RMD   	I can remove a distant directory
- [X] CLOS   	I can close the connection with the server (without crashing it)
- [X] PORT   	I can configure the port on which the server will listen
- [X] HOME   	I can configure the home directory of the server
- [ ] ACPA   	The server supports both Active and Passive mode
- [X] THRE      The server can handle multiple clients at the same time

You can also find an implementation of the THMB command, to use it you'll have to go through
the passive mode (as if you were about to download a file).

## Introduction

### What is LobsterFTP?

LobsterFTP is an FTP server written in Rust. This is a school project realised by Hugo Forraz 
for the course "Distributed Systems 1" at the University of Lille.  
The goal of this project is to create an FTP server that can handle multiple clients at the same time.

This project got his name while we were talking about it with another student, I needed something that
gave off the 2 main points of this project:
- FTP Server
- Written in Rust

When he said:
> What about LobsterFTP? I mean, Ferris (Rust's mascot) is a crustacean and "lobsters" are crustaceans 
> too, plus "lobster" sounds like "server" !

I was sold.

### How to use it?

To use this project, you need to have Rust installed on your computer. Please refer to the official
documentation to install it: https://www.rust-lang.org/tools/install.

> Please note that this project is only tested on Linux. It may work on Windows and MacOS but it is 
> not guaranteed.  
> **And note that this project was built using the Nightly toolchain, so you'll need to install the 
> nightly version of Rust.**

Once Rust is installed, you can clone this repository and run the following command in the root of 
the project to build the project :

```bash
cargo build --release
```

This will install all dependencies and build an executable in the `target/release` folder. Then
you'll just have to launch that executable like this :

```shell
lobster_ftp config.toml data/authorized_users.toml
```

The default config is the following one :

```toml
server_root = "LobsterRoot"
address = "127.0.0.1"
port = 54321
debug_level = 2 # 0 = trace, 1 = debug, 2 = info, 3 = warn, 4 = error, else anything
debug = false # true = also log to console, false = only log to file
```

You can change the config by changing the `config.toml` file in the root of the project.

If the directory given to `server_root` doesn't exist, the server will create it.

Please provide a full path without shortcuts (this particular one seems to not work : `~`) to 
the `server_root` directory.  
Please also note that all of those arguments are needed and the server won't be able to start 
if you don't provide one of them.

## Architecture

This project is in Rust, so I can't really use OOP to build it, but here's a list of 
the main components used to create some abstraction in this project:
- [Server](src/server.rs): allows to test the server without having to use an FTP client.
- [FileOverSocketCommand](src/command_handlers/upload_download.rs): allows an easier use of the
  of another socket to send or receive information.
- Enums; A lot of enums. Those allow to have a better abstraction of the different commands and 
  responses that can be sent by the server. You can find some in [FtpStatus](./src/status_codes.rs)
  or [FtpCommands](./src/commands.rs).

You can also find some OOP-like logic with the use of traits. For example, I used a template 
method pattern to handle the "classic" and "mocking" servers. You can also find a Builder pattern
to build the server without having a headache while reading the code.

Note that this project was built on top of `Tokio` a Rust library made for asynchronous servers,
in the real world, this project could have been built using Rocket or Actix or by even using the 
`ftp` crate but here it was more interesting to build the whole FTP implementation from scratch.

Here are all the crates used in this project:
- [`tokio`](https://crates.io/crates/tokio) - A runtime for writing reliable asynchronous 
  networking applications in Rust.
- [`clap`](https://crates.io/crates/clap) - A full featured, fast Command Line Argument Parser 
  for Rust, it makes the configuration a lot easier.
- [`log4rs`](https://crates.io/crates/log4rs) & [`log`](https://crates.io/crates/log) - A logging 
  framework for Rust, a distributed application is better if you know what happens.
- [`async_trait`](https://crate.io/crates/async_traits) add the possibility to make an asynchronous
  abstraction to the program... 
  + For example, it has been used for the server which has been written in a `trait`, the main implementation can be found in `src/server.rs` and the `async` implementation can be found in [`src/server/ftp_server.rs`](./src/server/ftp_server.rs) and the testing one in [`src/server/mock_server.rs`](./src/server/mock_server.rs).


## Code Samples

Here are some code samples that I found interesting in this project:

#### Communication with the client, upload and download

This code sample is from the [`RETR`](./src/command_handlers/upload_download/retr.rs) command but is nearly 
the same for the `STOR` command.  
It looks like some of my classmates had a hard time with the upload and download, with this code :

```rust
const BUFFER_SIZE: usize = 4096;
impl Retr {
    /// This is a function that is looped until the file is fully read.
    async fn read_and_write(&self, socket: &mut TcpStream, file: &mut File) -> io::Result<usize> {
        let mut buffer = [0; BUFFER_SIZE];
        let n = file.read(&mut buffer).await?;
        if n == 0 { return Ok(0); }
        socket.write_all(&buffer[..n]).await?;
        Ok(n) 
    }
    /* ... */
    async fn handle(&self, mut socket: TcpStream, path: &str) -> Result<CommandResult, CommandError> {
    /* ... */
        loop {
            match self.read_and_write(&mut socket, &mut file).await { 
                Ok(0) => break, // The file has been fully read, we can stop the loop.
                Ok(_) => continue,
                Err(_e) => {
                /* ... */
                }
            }
        }
    /* ... */ 
    }
}
```

With this piece of code, I had no issue with uploading neither downloading files. For example, I was able to 
upload and download a 3.2GB file (a Pop_os iso) in a bit less than 2 minutes. And without any like, loosing 
some bytes in the process. 

#### A simple yet efficient FTP server

This code sample is from the [`FtpServer`](./src/server/ftp_server.rs) implementation of the `Server` trait.
It doesn't do much, it just spawns a new thread for each client that connects to the server. Yet, for me,
it's a good example of how Rust works. At first, I was handling everything in this method, it was a bit
hard to read and to understand what was going on... We were at a point where we reached ~6 levels of
indentation in this method and more than 500 lines of code. 

So to make it more readable, anyone would have split the code into multiple methods. I think that a lot of
people would have done something like this:

```rust
impl Server {
    async fn on_connect(&self) { /* ... */ }
    async fn loop_content(&self, mut socket: TcpStream) { /* ... */ }
}
#[async_trait]
impl Server for FtpServer {
    /* ... */
  async fn handle_connection(&self, socket: TcpStream) {
    let _task = tokio::spawn(async move {
      self.on_connect().await;
      loop {
        self.loop_content().await;
      }
    });
  }
}
```

But here, we can't do that. The reason is simple, the `move` keyword is used in the `tokio::spawn` method.
This means that the `self` variable is moved into the new thread. So we can't use it anymore... But, if I
can't use `self` anymore, how can I call the `on_connect` and `loop_content` methods for other clients? 

`rustc` (the compiler) would tell you to do something about that (and stop compiling the code). So to solve 
this issue, I created a new struct called `Client` which contains all the data needed to handle a client.
And I moved the `on_connect` and `loop_content` methods into this `struct`'s implementation :

```rust
pub struct Client { /* ... */ }

impl Client {
  pub fn new(socket: TcpStream, authorized_users: Arc<Vec<AuthorizedUser>>, root: &str) -> Client {
    Self { /* ... */ }
  }
  async fn on_connect(&mut self) -> Result<(), CommandError> { /* ... */ }
  async fn loop_content(&mut self) -> Result<(), CommandError> {
    loop { /* ... */ }
  }
  
  /* ... */
}

```

And with that, I just needed to make a new `Client` for each client that connects to the server. You
can also note that I used an `Arc<Vec<AuthorizedUser>>` instead of a `Vec<AuthorizedUser>`. This is
because, we want a client to be able to connect as any user, so we need to share the list of authorized
users... But we don't want to clone the whole list for each client, so we use an `Arc` (Atomic Reference
Counted) to share the list between all the clients.

You can also note those `if let Err(e) = /* ... */` statements. This is a way to handle errors in Rust.
When you have an Enum (like Result or Option), you can use the `if let` statement to match one variant
of the Enum. If the variant matches, the code inside the `if let` block is executed and you can even bind
the value of the variant to a variable. Of course, if the variant doesn't match, the code inside the
`if let` block is not executed.

```rust
#[async_trait]
impl Server for FtpServer {
    /* ... */

  async fn handle_connection(&self, socket: TcpStream) {
    let mut client = Client::new(socket, Arc::clone(&self.authorized_users), &self.root);
    let _task = tokio::spawn(async move {
      let result_on_connect = client.on_connect().await;
      if let Err(e) = result_on_connect { error!("Error on connect: {}\n", e); }
      let result_loop_content = client.loop_content().await;
      if let Err(e) = result_loop_content {  error!("Error while looping content: {}\n", e); }
    });
  }
}
```

Finally, here is the last code sample I find interesting. This method is from the `Client` struct. It
handles the commands sent by the client. It's a bit long, but I think it's another good example of how
Rust works. 

Here, you can see a few things like :
- the `match` statement, it works a lot like the `switch` statement in other languages. The biggest 
    difference is that you can even bind the value of the variant to a variable.
- the `?` operator, it's a way to handle errors in Rust. If you have a `Result` type, you can use the
    `?` operator to propagate the error to the caller rather than handling it in the current function.
- the lack of `;` at the end of some lines, this is because Rust returns the value of the last line of
    a block So if you don't want to return the value of the last line, you can add a `;` at the
    end of the line.

```rust
impl Client {
    /* ... */
    async fn dispatch_command(&mut self, command: Result<FtpCommands, MessageParsingError>) 
      -> Result<FtpStatus, CommandError> {
        self.abort_if_issue(&command)?;
        let match_result =match command {
            Ok(command) => {
                let result = self.dispatcher(&command).await;
                self.last_command = command;
                result?
            }
            Err(error) => match error {
                MessageParsingError::CommandDoesNotExist => FtpStatus::CommandNotImplementedUnknown,
                MessageParsingError::EmptyMessage => FtpStatus::SyntaxErrorInParametersOrArguments,
                MessageParsingError::LacksParameters => {
                    FtpStatus::CommandNotImplementedForThatParameter
                }
                _ => FtpStatus::RequestedActionNotTaken,
            },
        };
        Ok(match_result)
    }
}
```

## Details on the points

### CMP

You can check if the code compiles using :

```shell
cargo build --release
```

### DOC

You'll find documentation in this file. You can generate the code's documentation using :

```shell
cargo doc --open
```

### TST

You can run the tests using :

```shell
cargo test
```

### COO

Here are a few design patterns used :

- Trait usage
- Builder
- Template method

### EXE

You can run this project using 

```shell
lobster_ftp config.toml data/authorized_users.toml
```

Please note that this command should be run as root if you want to use the default port (21).

### CON

You can try to connect to this server using FileZilla. It also works with the `treeftp`
command line made earlier.

### BADU

You will issue an error if you try to connect with a user that is not in the list of authorized users.

### BADP

You will issue an error if you try to connect with a password that is not the one associated with the 
user you are trying to connect with.

### LST

The command `LIST` as been implemented.

### CWD

The command `CWD` as been implemented.

### CDUP

The command `CDUP` as been implemented.

### ROOT

You are not allowed to go above the root directory.

### RENF

You can rename a file using the `RNFR` and `RNTO` commands.

### MKD

You can create a directory using the `MKD` command.

### RMD

You can remove a directory using the `RMD` or the directory and all its content using the `RMDA` commands.

### CLOS

Errors have been handled to disallow the possibility to crash the server.

### PORT

You can change the port used by the server in the configuration file.

### HOME

You can set the root directory of the server in the configuration file.

### ACPA

Not done.

### THRE

This is handle by the `tokio` library used in this project. It allows the server to handle multiple clients at the same time. 
We handle this with the following instruction : 

```rust
tokio::spawn(async move {
    handle_connection(socket).await;
});
```

Here, we literally ask tokio to create a thread and give the ownership of our socket (we can't use it in the main thread anymore¹) to an asynchronous function called `handle_connection` and let this thread wait for the resolution of this function (reason of the `await` keyword in this sample).

> ¹Here, we can work around this situation using an `Arc` and a `Mutex` to share a mutable ownership of the socket, that would have been a reasonable solution if we were making a chat... But in our case, we should not need to share it's ownership.
