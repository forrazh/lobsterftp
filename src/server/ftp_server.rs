use crate::AuthorizedUser;
use async_trait::async_trait;
use log::{error, info};
use std::sync::Arc;
use tokio::io;
use tokio::net::{TcpListener, TcpStream};

use crate::client::Client;
use crate::server::{Server, PORT};

pub struct FtpServer {
    address: String,
    port: u16,
    authorized_users: Arc<Vec<AuthorizedUser>>,
    pub root: String,
}

impl FtpServer {}

pub struct FtpServerBuilder {
    address: Option<String>,
    port: Option<u16>,
    authorized_users: Option<Vec<AuthorizedUser>>,
    root: Option<String>,
}

#[async_trait]
impl Server for FtpServer {
    async fn init_tcp_listener(&self) -> io::Result<TcpListener> {
        let address = format!("{}:{}", self.address, self.port);
        info!("Starting server on {}\n", address);
        TcpListener::bind(address).await
    }

    async fn handle_connection(&self, socket: TcpStream) {
        let mut client = Client::new(socket, Arc::clone(&self.authorized_users), &self.root);
        let _task = tokio::spawn(async move {
            let result_on_connect = client.on_connect().await;
            if let Err(e) = result_on_connect {
                error!("Error on connect: {}\n", e);
            }
            let result_loop_content = client.loop_content().await;
            if let Err(e) = result_loop_content {
                error!("Error while looping content: {}\n", e);
            }
        });
    }
}

impl FtpServerBuilder {
    pub fn new() -> Self {
        FtpServerBuilder {
            address: None,
            port: None,
            authorized_users: None,
            root: None,
        }
    }

    pub fn address(mut self, address: String) -> Self {
        self.address = Some(address);
        self
    }

    pub fn port(mut self, port: u16) -> Self {
        self.port = Some(port);
        self
    }

    pub(crate) fn authorized_users(mut self, p0: Vec<AuthorizedUser>) -> Self {
        self.authorized_users = Some(p0);
        self
    }

    pub(crate) fn root(mut self, root: String) -> Self {
        self.root = Some(root);
        self
    }

    pub fn build(self) -> FtpServer {        
        FtpServer {
            address: self.address.unwrap_or_else(|| "127.0.0.1".to_string()),
            port: self.port.unwrap_or(PORT),
            authorized_users: Arc::new(self.authorized_users.unwrap_or_default()),
            root: self.root.unwrap_or_else(|| ".".to_string()),
        }
    }
}
