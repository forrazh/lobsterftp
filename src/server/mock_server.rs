#[cfg(test)]
mod tests {
    use crate::server::Server;
    use async_trait::async_trait;
    use std::sync::{Arc, Mutex};
    use tokio::io;
    use tokio::io::{AsyncReadExt, AsyncWriteExt};
    use tokio::net::{TcpListener, TcpStream};

    static mut CAN_RUN: bool = true;

    struct MockServer {
        port: u16,
    }

    impl MockServer {
        fn new(port: u16) -> MockServer {
            unsafe {
                CAN_RUN = true;
            }
            MockServer { port }
        }
    }

    #[async_trait]
    impl Server for MockServer {
        async fn init_tcp_listener(&self) -> io::Result<TcpListener> {
            TcpListener::bind(format!("127.0.0.1:{}", self.port)).await
        }
        async fn handle_connection(&self, mut socket: TcpStream) {
            tokio::spawn(async move {
                println!("MockServer::handle_connection");

                let mut buf = [0; 1024];

                let n = socket.read(&mut buf).await.unwrap();
                println!("Received: {}", String::from_utf8_lossy(&buf[..n]));

                let message = "Response from mock server".as_bytes();
                socket.write_all(message).await.unwrap();
            });
        }

        fn can_run(&self) -> bool {
            unsafe {
                if CAN_RUN {
                    return true;
                }
            }
            false
        }
    }

    #[tokio::test]
    async fn test_mock_server() {
        const PORT: u16 = 54321;
        const ADDRESS: &str = "127.0.0.1:54321";
        let server = MockServer::new(PORT);
        let server_task = tokio::spawn(async move {
            server
                .run_server()
                .await
                .expect("Error while creating the server");
        });

        println!("Connecting to server...");
        tokio::time::sleep(tokio::time::Duration::from_millis(500)).await;
        let mut stream = TcpStream::connect(ADDRESS.to_string()).await.unwrap();
        let request = "Request from test client";
        stream.write_all(request.as_bytes()).await.unwrap();

        let mut response = String::new();
        stream.read_to_string(&mut response).await.unwrap();
        assert_eq!(response, "Response from mock server");
        server_task.abort();
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 10)]
    async fn test_mock_server_handles_multiple_connections() {
        const PORT: u16 = 54322;
        const ADDRESS: &str = "127.0.0.1:54322";
        let server = MockServer::new(PORT);
        let server_task = tokio::spawn(async move {
            server.run_server().await.unwrap();
        });

        tokio::time::sleep(tokio::time::Duration::from_millis(500)).await;

        let n_connections = 10;
        let responses = Arc::new(Mutex::new(vec![]));
        for i in 0..n_connections {
            let my_responses = Arc::clone(&responses);
            tokio::spawn(async move {
                // let my_responses = my_responses;
                println!("Connecting to server...({}/{})", i, n_connections);
                let mut stream = TcpStream::connect(ADDRESS.to_string()).await.unwrap();
                let request = format!("Request {} from test client", i);
                stream.write_all(request.as_bytes()).await.unwrap();

                let mut response = String::new();
                stream.read_to_string(&mut response).await.unwrap();
                {
                    let mut r = my_responses.lock().unwrap();
                    r.push(response);
                }
                // stream.shutdown().await.expect("Error while shutting down the stream");
            })
            .await
            .expect("Error while spawning the task");
        }

        unsafe {
            CAN_RUN = false;
        }

        let responses = responses.lock().unwrap();
        for i in 0..n_connections {
            assert_eq!(responses[i], "Response from mock server");
        }
        server_task.abort();
    }
}
