use crate::errors::command_errors::CommandError;

pub mod cwd;
pub mod upload_download;

#[allow(dead_code)]
#[derive(Debug, PartialEq, Eq)]
pub enum CommandResult {
    FileList { files: Vec<String> },
    ChangedDirectory { new_path: String },
    FileUploaded,
    FileDownloaded,
    DirectoryCreated,
    DirectoryDeleted,
    FileDeleted,
    FileRenamed,
    DirectoryRenamed,
}

pub trait FileCommandHandler {
    fn handle(&self, path: &str) -> Result<CommandResult, CommandError>;
}

pub trait CommunicatingCommandHandler {
    fn handle(&self) -> Result<CommandResult, CommandError>;
}
