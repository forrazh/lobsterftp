use std::net::IpAddr;
use std::path::Path;
use std::sync::Arc;

use log::{debug, error, info};
use tokio::io;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};

use crate::command_handlers::cwd::CWD;
use crate::command_handlers::upload_download::list::List;
use crate::command_handlers::upload_download::mkd::Mkd;
use crate::command_handlers::upload_download::retr::Retr;
use crate::command_handlers::upload_download::stor::{Stor, Stou};
use crate::command_handlers::upload_download::{CommandWithPath, FileOverSocketCommand};
use crate::command_handlers::{CommandResult, FileCommandHandler};
use crate::commands::{FileType, FtpCommands, ModeType};
use crate::errors::command_errors::CommandError;
use crate::errors::message_parsing::MessageParsingError;
use crate::status_codes::FtpStatus;
use crate::AuthorizedUser;
use crate::command_handlers::upload_download::delete::Delete;
use crate::command_handlers::upload_download::rename::Renamer;
use crate::command_handlers::upload_download::thumb::Thumb;

/// The client struct is used to handle the communication with a single client.
/// It contains the socket and all the context needed to handle the client's
/// requests.
pub struct Client {
    socket: TcpStream,
    current_location: String,
    username: Option<String>,
    password: Option<String>,
    logged_in: bool,
    last_command: FtpCommands,
    authorized_users: Arc<Vec<AuthorizedUser>>,
    root: String,
    _current_type: FileType,
    _current_mode: ModeType,
    other_socket: Option<TcpListener>,
}

impl Client {
    pub fn new(
        socket: TcpStream,
        authorized_users: Arc<Vec<AuthorizedUser>>,
        root: &str,
    ) -> Client {
        Client {
            socket,
            current_location: String::from("/"),
            username: None,
            password: None,
            logged_in: false,
            last_command: FtpCommands::NOOP,
            authorized_users,
            root: String::from(root),
            _current_type: FileType::ASCII,
            _current_mode: ModeType::Stream,
            other_socket: None,
        }
    }

    fn get_full_path(&self) -> String {
        format!("{}/{}", self.root, self.current_location)
    }

    /// This function is used to send a message to the client.
    async fn write_socket(&mut self, message: &str) -> io::Result<usize> {
        info!("Sending: {}", message);
        self.socket.write(message.as_bytes()).await
    }

    /// This function is used to send a status message to the client.
    async fn write_status_to_socket(&mut self, status: FtpStatus) -> io::Result<usize> {
        self.write_socket(status.as_string().as_str()).await
    }

    /// This function is used to read a message from the client.
    async fn read_socket(&mut self, buffer: &mut [u8; 1024]) -> Option<String> {
        match self.socket.read(buffer).await {
            Ok(0) => {
                info!("Connection closed\n");
                None
            }
            Ok(n) => {
                info!("Read {} bytes\n", n);
                let message = String::from_utf8_lossy(&buffer[..n]);
                info!("Received: {}\n", message);
                Some(message.parse().unwrap())
            }
            Err(e) => {
                error!("Error reading from socket: {}\n", e);
                None
            }
        }
    }

    /// Sends a message to the client when he first connects.
    pub async fn on_connect(&mut self) -> io::Result<()> {
        info!("New connection from {}\n", self.socket.peer_addr().unwrap());
        let _result_service_ready = self.write_status_to_socket(FtpStatus::ServiceReady).await?;
        // let _result_name_system_type = self.write_status_to_socket(FtpStatus::NameSystemType).await?;
        Ok(())
    }

    /// Loop client requests until the client disconnects.
    pub async fn loop_content(&mut self) -> io::Result<()> {
        let mut buffer = [0; 1024];
        while let Some(message) = self.read_socket(&mut buffer).await
            && message != "quit" {
            {
                let command = FtpCommands::parse_message(message.as_str());
                let message_to_send = self.dispatch_command(command).await;
                match message_to_send {
                    Ok(message) => {
                        self.write_status_to_socket(message).await?;
                    }
                    Err(e) => {
                        let msg = format!("Error: {}\n", e);
                        error!("{}", msg);
                        self.write_socket(&msg).await?;
                    }
                }
            }
        }
        Ok(())
    }

    /// Handles the communication with the second socket.
    /// This is used for the STOR, RETR, LIST and STOU commands.
    /// The second socket is used to transfer the data.
    /// The first socket is used to send the status of the command.
    async fn call_other_socket(
        &mut self,
        command: Box<impl FileOverSocketCommand>,
        string_data: String,
        succeed_status: FtpStatus,
    ) -> Result<FtpStatus, CommandError> {
        match self.other_socket {
            Some(ref mut other_socket) => {
                info!("Calling other socket\n");
                let (socket, _) = other_socket.accept().await.map_err(|_| CommandError::Ded)?;
                let _result = command.handle(socket, &string_data).await?;
                self.write_status_to_socket(FtpStatus::FileStatusOk)
                    .await
                    .map_err(|e| {
                        error!("Error writing to socket: {}", e);
                        CommandError::Ded
                    })?;
                self.other_socket = None;
                Ok(succeed_status.clone())
            }
            None => Err(CommandError::Ded),
        }
    }

    /// This function is used to dispatch the commands to the right function.
    async fn dispatcher(&mut self, command: &FtpCommands) -> Result<FtpStatus, CommandError> {
        let status = match command {
            FtpCommands::CDUP => {
                let dir_event =
                    CWD::new(self.root.clone(), self.current_location.clone()).handle("..");
                match dir_event {
                    Ok(CommandResult::ChangedDirectory { new_path }) => {
                        self.current_location = new_path;
                        FtpStatus::RequestedFileActionOk
                    }
                    _ => FtpStatus::RequestedActionNotTakenFileUnavailable,
                }
            }
            FtpCommands::CWD(path) => {
                let dir_event = CWD::new(self.root.clone(), self.current_location.clone())
                    .handle(&path.clone().unwrap_or_default());
                match dir_event {
                    Ok(CommandResult::ChangedDirectory { new_path }) => {
                        self.current_location = new_path;
                        FtpStatus::RequestedFileActionOk
                    }
                    _ => FtpStatus::RequestedActionNotTakenFileUnavailable,
                }
            }
            FtpCommands::DELE(path) => {
                let command = Delete::new(self.root.clone(), self.current_location.clone());
                command.delete_file(path).await?;
                FtpStatus::RequestedFileActionOk
            },
            FtpCommands::FEAT => FtpStatus::Features,
            FtpCommands::HELP => FtpStatus::HelpMessage,
            FtpCommands::LIST => {
                let list_command = List::new(self.root.clone(), self.current_location.clone());
                self.call_other_socket(
                    Box::new(list_command),
                    self.current_location.to_string(),
                    FtpStatus::ClosingDataConnection,
                )
                .await?
            }
            FtpCommands::MKD(path) => {
                let command = Mkd::new(self.root.clone(), self.current_location.clone());
                command.handle(path).await?;
                FtpStatus::PathnameCreated(path.clone(), "created".to_string())
            }
            FtpCommands::NOOP => FtpStatus::SystemStatus("".to_string()),
            FtpCommands::PASS(password) => {
                if self.logged_in {
                    FtpStatus::UserAlreadyLoggedIn
                } else if self.password.as_ref().is_some_and(|pass| pass == password) {
                    self.logged_in = true;
                    info!(
                        "User {:?} logged in at {:?}\n",
                        self.username,
                        self.socket.peer_addr()
                    );
                    FtpStatus::UserLoggedIn
                } else {
                    FtpStatus::NotLoggedIn
                }
            }
            FtpCommands::EPSV | FtpCommands::PASV(_) => {
                let address = self.socket.local_addr().unwrap();
                let [first, second, third, fourth] = {
                    match address.ip() {
                        IpAddr::V4(ip) => ip.octets(),
                        IpAddr::V6(_ip) => return Err(CommandError::Ded),
                    }
                };
                let address = format!("{first}.{second}.{third}.{fourth}:0");
                let listener = TcpListener::bind(address)
                    .await
                    .map_err(|e| {
                        error!("Error occured: {}", e);
                        CommandError::Ded
                    })?;
                let full_port = listener.local_addr().map_err(|e| {
                    error!("Error occured: {}", e);
                    CommandError::Ded
                })?.port();
                self.other_socket = Some(listener);
                let lower: u8 = (full_port % 256) as u8;
                let upper: u8 = (full_port / 256) as u8;
                let address = format!("({first},{second},{third},{fourth},{upper},{lower})");
                FtpStatus::EnteringPassiveMode(address)
            }
            FtpCommands::PWD => FtpStatus::PathnameCreated(
                self.current_location.clone(),
                "is the current directory".to_string(),
            ),
            FtpCommands::QUIT => FtpStatus::ServiceClosingControlConnection,
            FtpCommands::RETR(path) => {
                let retr = Retr::new(self.root.clone(), self.current_location.clone());
                self.call_other_socket(
                    Box::new(retr),
                    path.clone(),
                    FtpStatus::ClosingDataConnection,
                )
                .await?
            }
            FtpCommands::RMD(path) => {
                let command = Delete::new(self.root.clone(), self.current_location.clone());
                command.delete_directory(path).await?;
                FtpStatus::RequestedFileActionOk
            },
            FtpCommands::RMDA(path) => {
                let command = Delete::new(self.root.clone(), self.current_location.clone());
                command.delete_directory_and_contents(path).await?;
                FtpStatus::RequestedFileActionOk
            },
            FtpCommands::RNFR(path) => {
                let full_path = Renamer::new(self.root.clone(), self.current_location.clone()).get_full_path(path);
                if Path::new(&full_path).exists() {
                    FtpStatus::RequestedFileActionPendingFurtherInformation
                } else {
                    FtpStatus::RequestedActionNotTakenFileUnavailable
                }
            },
            FtpCommands::RNTO(path) => {
                match &self.last_command {
                    FtpCommands::RNFR(old_path) => {
                        let to = Renamer::new(
                            self.root.clone(),
                            self.current_location.clone(),
                        );
                        let from_path = to.get_full_path(old_path);
                        let to_path = to.get_full_path(path);
                        tokio::fs::rename(from_path, to_path).await.map_err(|e| {
                            error!("Error renaming file: {:?}", e);
                            CommandError::Ded
                        })?;
                        FtpStatus::RequestedFileActionOk
                    }
                    _ => FtpStatus::BadCommandSequence(Some("Expected RNFR before this".to_string())),
                }
            },
            FtpCommands::STOR(path) => {
                let command = Stor::new(self.root.clone(), self.current_location.clone());
                self.call_other_socket(
                    Box::new(command),
                    path.clone(),
                    FtpStatus::ClosingDataConnection,
                )
                .await?
            }
            FtpCommands::STOU(path) => {
                let command = Stou::new(self.get_full_path(), self.current_location.clone());
                self.call_other_socket(
                    Box::new(command),
                    path.clone(),
                    FtpStatus::ClosingDataConnection,
                )
                .await?
            }
            FtpCommands::SYST => FtpStatus::NameSystemType,
            FtpCommands::THMB(path) => {
                let command = Thumb::new(self.root.clone(), self.current_location.clone());
                self.call_other_socket(
                    Box::new(command),
                    path.clone(),
                    FtpStatus::ClosingDataConnection,
                )
                .await?
            },
            FtpCommands::USER(username) => {
                if self.logged_in {
                    FtpStatus::UserAlreadyLoggedIn
                } else if let Some(user) = self
                    .authorized_users
                    .iter()
                    .find(|user| &user.name == username)
                {
                    self.username = Some(user.name.clone());
                    self.password = Some(user.pass.clone());
                    FtpStatus::UserNameOkay
                } else {
                    FtpStatus::NeedAccountForLogin
                }
            }
            _ => FtpStatus::CommandNotImplementedUnused,
        };
        Ok(status)
    }

    /// dispatch a command to the correct handler
    async fn dispatch_command(
        &mut self,
        command: Result<FtpCommands, MessageParsingError>,
    ) -> Result<FtpStatus, CommandError> {
        self.abort_if_issue(&command)?;
        Ok(match command {
            Ok(command) => {
                let result = self.dispatcher(&command).await;
                self.last_command = command;
                result?
            }
            Err(error) => match error {
                MessageParsingError::CommandDoesNotExist => FtpStatus::CommandNotImplementedUnknown,
                MessageParsingError::EmptyMessage => FtpStatus::SyntaxErrorInParametersOrArguments,
                MessageParsingError::LacksParameters => {
                    FtpStatus::CommandNotImplementedForThatParameter
                }
                _ => FtpStatus::RequestedActionNotTaken,
            },
        })
    }

    /// issue an error if the command is not allowed in the current state
    fn abort_if_issue(
        &self,
        command: &Result<FtpCommands, MessageParsingError>,
    ) -> Result<(), CommandError> {
        debug!(
            "Last command: {:?}; Current command: {:?}\n",
            self.last_command, command
        );
        if matches!(self.last_command, FtpCommands::USER(_)) {
            if let Ok(FtpCommands::USER(_)) = command {
                return Ok(());
            }
            if let Ok(FtpCommands::PASS(_)) = command {
                return Ok(());
            }
            return Err(CommandError::LoginFirst(
                "You must send PASS after USER".to_string(),
            ));
        }

        if !self.logged_in {
            if let Ok(FtpCommands::USER(_)) = command {
                return Ok(());
            }
            return Err(CommandError::LoginFirst(
                "You must start with USER".to_string(),
            ));
        }

        Ok(())
    }
}
