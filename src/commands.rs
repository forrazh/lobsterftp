use crate::errors::message_parsing::MessageParsingError;

/// This module contains the commands that can be sent to the server.
/// The commands are defined in the RFC 959.
#[derive(Debug, PartialEq, Clone)]
pub enum FtpCommands {
    /// Abort an active file transfer
    /// Unimplemented
    ABOR,
    /// Account information.
    ACCT,
    /// Authentication/Security Data (RFC 2228)
    /// Unimplemented
    ADAT,
    /// Allocate sufficient disk space to receive a file.
    /// Unimplemented
    ALLO,
    /// Append (with create).
    /// If the file does not exist at the server site, it is created.
    APPE(String),
    /// Authentication/Security Mechanism (RFC 2228)
    /// Unimplemented
    AUTH,
    /// Get the Available space
    AVBL,
    /// Clear Command Channel
    CCC,
    /// Change to Parent Directory
    CDUP,
    /// Confidentiality Protection Command (RFC 2228)
    /// Unimplemented
    CONF,
    /// Client / Server Identification
    /// Unimplemented
    CSID,
    /// Change Working Directory
    /// If no directory is specified, the server should return to the root directory.
    CWD(Option<String>),
    /// Delete File
    /// Parameter should be a file name.
    /// If the file is a symbolic link, the link itself is deleted.
    /// If the file is a directory, the directory must be empty for it to be deleted.
    DELE(String),
    /// Get the directory size
    /// Unimplemented
    DSIZ,
    /// Privacy Protected Channel (RFC 2228)
    /// Unimplemented
    ENC,
    /// Specifies an extended address and port to which the server should connect.
    /// Unimplemented
    EPRT,
    /// Enter Extended Passive Mode
    /// Unimplemented
    EPSV,
    /// Get the feature list implemented by the server
    FEAT,
    /// Returns usage documentation on a command if specified, else a general help document is returned.
    HELP,
    /// Identify desired virtual host on server, by name
    /// Unimplemented
    HOST,
    /// Language Negotiation (RFC 2640)
    /// Unimplemented
    LANG,
    /// Lists the contents of a directory (if a directory is named) or lists the status of a file (if a file is named).
    LIST,
    /// Specifies a long address and port to which the server should connect.
    /// Unimplemented
    LPRT,
    /// Enter Long Passive Mode
    /// Unimplemented
    LPSV,
    /// Returns the last-modified time of a specified file.
    /// Unimplemented
    MDTM,
    /// Modify the creation time of a file
    /// Unimplemented
    MFCT,
    /// Modify fact of a file (the last-modified time, creation time, UNIX group, UNIX mode, etc.)
    /// Unimplemented
    MFF,
    /// Modify the last-modified time of a file
    /// Unimplemented
    MFMT,
    /// Integrity Protected Command (RFC 2228)
    /// Unimplemented
    MIC,
    /// Make Directory
    /// Parameter should be a directory name.
    MKD(String),
    /// Lists the contents of a directory in a machine-processable form.
    /// Unimplemented
    MLSD,
    /// Provides data about exactly what the server is sending over the data connection.
    /// Unimplemented
    MLST,
    /// Change Mode
    /// Parameter should be a representation type.
    /// Must be one of S (Stream), B (Block), C (Compressed).
    MODE(ModeType),
    /// Returns a list of files in a specified directory.
    /// If no directory is specified, the contents of the current directory are returned.
    NLST(Option<String>),
    /// No Operation
    NOOP,
    /// Select options for a feature
    OPTS,
    /// Authentification password
    /// Parameter should be a password.
    PASS(String),
    /// Enter passive mode
    /// Optional host and port parameters can be used to direct the server to connect to them.
    PASV(Option<String>),
    /// Protection Buffer Size (RFC 2228)
    /// Unimplemented
    PBSZ,
    /// Specifies an address and port to which the server should connect.
    /// Unimplemented
    PORT(String),
    /// Data Channel Protection Level (RFC 2228)
    /// Unimplemented
    PROT,
    /// Print Working Directory
    PWD,
    /// Quits the FTP session.
    QUIT,
    /// Reinitialize the connection
    /// Unimplemented
    REIN,
    /// Restarts a file transfer from the specified point.
    /// Parameter should be a marker at which to resume.
    REST,
    /// If the file is a symbolic link, the link itself is retrieved.
    /// Retrieve a copy of the file.
    /// Parameter should be a file name.
    /// If the file is a directory, the server should return a list of files in the directory.
    RETR(String),
    /// Remove a directory
    /// Parameter should be a directory name.
    /// The directory must be empty for it to be deleted.
    RMD(String),
    /// Remove a directory and all of its contents
    /// Parameter should be a directory name.
    RMDA(String),
    /// Rename from
    /// Parameter should be an existing file name.
    /// This command must be immediately followed by a `RNTO` command specifying the new name for the file.
    RNFR(String),
    /// Rename to
    /// Parameter should be a new file name.
    /// This command must be immediately preceded by a `RNFR` command specifying the old name for the file.
    RNTO(String),
    /// Sends site specific commands to remote server.
    /// Parameter should be a site command.
    /// The response returned by the server will depend on the command sent.
    /// Unimplemented
    SITE(String),
    /// Mount file structure
    /// Unimplemented
    SMNT,
    /// Use single port passive mode (only one TCP port number for both control connections and passive-mode data connections)
    /// Unimplemented
    SPSV,
    /// Returns information about the server status, including the status of the current connection.
    /// If a parameter is specified, the server should return information about that parameter.
    /// If no parameter is specified, the server should return general status information.
    STAT(Option<String>),
    /// Accept the data and to store the data as a file at the server site.
    /// If the file already exists at the server site, then its contents are replaced by the data being transferred.
    /// If the file does not exist at the server site, then a new file is created at the server site to hold the data.
    /// The name of the file at the server site is taken from the pathname specified in the pathname argument.
    STOR(String),
    /// Store a file uniquely
    /// If a file is successfully created, the reply shall be 250.
    /// If the file already exists, the reply shall be 452.
    /// If the file exists and is a directory, the reply shall be 553.
    /// If the file exists and is a symbolic link, the reply shall be 553.
    STOU(String),
    /// Set file transfer structure
    /// Unimplemented
    STRU(String),
    /// Returns the system type.
    SYST,
    /// Get a thumbnail of a remote image file
    /// The parameter should be a file name.
    /// Binary data is returned.
    /// The image is scaled to fit within a 128x128 pixel box.
    THMB(String),
    /// Set transfer mode (ASCII/Binary)
    /// Parameter should be a representation type.
    /// Must be one of A (ASCII), I (Image).
    TYPE(FileType),
    /// Authentification username
    /// Parameter should be a username.
    USER(String),
}

fn get_next_item_or_error<'a>(iter: &mut impl Iterator<Item=&'a str>) -> Result<&'a str, MessageParsingError> {
    iter.next().ok_or(MessageParsingError::LacksParameters)
}

impl FtpCommands {
    /// Parse a string to get the ftp command used
    pub(crate) fn parse_message(message: &str) -> Result<FtpCommands, MessageParsingError> {
        if message.is_empty() {
            return Err(MessageParsingError::EmptyMessage);
        }

        let mut split_message = message.split_whitespace();
        if let Some(msg) = split_message.next() {
            match msg {
                "ABOR" => Ok(FtpCommands::ABOR),
                "ACCT" => Ok(FtpCommands::ACCT),
                "ADAT" => Ok(FtpCommands::ADAT),
                "ALLO" => Ok(FtpCommands::ALLO),
                "APPE" => Ok(FtpCommands::APPE(get_next_item_or_error(&mut split_message)?.to_string())),
                "AUTH" => Ok(FtpCommands::AUTH),
                "AVBL" => Ok(FtpCommands::AVBL),
                "CCC"  => Ok(FtpCommands::CCC),
                "CDUP" => Ok(FtpCommands::CDUP),
                "CONF" => Ok(FtpCommands::CONF),
                "CSID" => Ok(FtpCommands::CSID),
                "CWD"  => Ok(FtpCommands::CWD(split_message.next().map(|item| item.to_string()))),
                "DELE" => Ok(FtpCommands::DELE(get_next_item_or_error(&mut split_message)?.to_string())),
                "DSIZ" => Ok(FtpCommands::DSIZ),
                "ENC"  => Ok(FtpCommands::ENC),
                "EPRT" => Ok(FtpCommands::EPRT),
                "EPSV" => Ok(FtpCommands::EPSV),
                "FEAT" => Ok(FtpCommands::FEAT),
                "HELP" => Ok(FtpCommands::HELP),
                "HOST" => Ok(FtpCommands::HOST),
                "LANG" => Ok(FtpCommands::LANG),
                "LIST" => Ok(FtpCommands::LIST),
                "LPRT" => Ok(FtpCommands::LPRT),
                "LPSV" => Ok(FtpCommands::LPSV),
                "MDTM" => Ok(FtpCommands::MDTM),
                "MFCT" => Ok(FtpCommands::MFCT),
                "MFF"  => Ok(FtpCommands::MFF),
                "MFMT" => Ok(FtpCommands::MFMT),
                "MIC"  => Ok(FtpCommands::MIC),
                "MKD"  => Ok(FtpCommands::MKD(get_next_item_or_error(&mut split_message)?.to_string())),
                "MLSD" => Ok(FtpCommands::MLSD),
                "MLST" => Ok(FtpCommands::MLST),
                "MODE" => {
                    let mode = split_message.next()
                        .map(|item| ModeType::from_string(item))
                        .ok_or_else(|| MessageParsingError::LacksParameters)??;
                    Ok(FtpCommands::MODE(mode))
                }
                "NLST" => Ok(FtpCommands::NLST(split_message.next().map(|item| item.to_string()))),
                "NOOP" => Ok(FtpCommands::NOOP),
                "OPTS" => Ok(FtpCommands::OPTS),
                "PASS" => Ok(FtpCommands::PASS(get_next_item_or_error(&mut split_message)?.to_string())),
                "PASV" => Ok(FtpCommands::PASV(split_message.next().map(|item| item.to_string()))),
                "PBSZ" => Ok(FtpCommands::PBSZ),
                "PORT" => Ok(FtpCommands::PORT(get_next_item_or_error(&mut split_message)?.to_string())),
                "PROT" => Ok(FtpCommands::PROT),
                "PWD"  => Ok(FtpCommands::PWD),
                "QUIT" => Ok(FtpCommands::QUIT),
                "REIN" => Ok(FtpCommands::REIN),
                "REST" => Ok(FtpCommands::REST),
                "RETR" => Ok(FtpCommands::RETR(get_next_item_or_error(&mut split_message)?.to_string())),
                "RMD"  => Ok(FtpCommands::RMD(get_next_item_or_error(&mut split_message)?.to_string())),
                "RMDA" => Ok(FtpCommands::RMDA(get_next_item_or_error(&mut split_message)?.to_string())),
                "RNFR" => Ok(FtpCommands::RNFR(get_next_item_or_error(&mut split_message)?.to_string())),
                "RNTO" => Ok(FtpCommands::RNTO(get_next_item_or_error(&mut split_message)?.to_string())),
                "SITE" => Ok(FtpCommands::SITE(get_next_item_or_error(&mut split_message)?.to_string())),
                "SMNT" => Ok(FtpCommands::SMNT),
                "SPSV" => Ok(FtpCommands::SPSV),
                "STAT" => Ok(FtpCommands::STAT(split_message.next().map(|item| item.to_string()))),
                "STOR" => Ok(FtpCommands::STOR(get_next_item_or_error(&mut split_message)?.to_string())),
                "STOU" => Ok(FtpCommands::STOU(get_next_item_or_error(&mut split_message)?.to_string())),
                "STRU" => Ok(FtpCommands::STRU(get_next_item_or_error(&mut split_message)?.to_string())),
                "SYST" => Ok(FtpCommands::SYST),
                "THMB" => Ok(FtpCommands::THMB(get_next_item_or_error(&mut split_message)?.to_string())),
                "TYPE" => {
                    let file_type = split_message.next()
                        .map(FileType::from_string)
                        .ok_or(MessageParsingError::LacksParameters)??;
                    Ok(FtpCommands::TYPE(file_type))
                }
                "USER" => Ok(FtpCommands::USER(get_next_item_or_error(&mut split_message)?.to_string())),
                _ => Err(MessageParsingError::CommandDoesNotExist)
            }
        } else {
            Err(MessageParsingError::EmptyMessage)
        }
    }
}

/// Enum used to specify the kind of file to transfer.
/// ASCII is the default.
/// Binary is used for files that contain binary data like images.
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum FileType {
    ASCII,
    Binary,
}

/// Enum used to specify the type of file transfer.
/// Stream is the default.
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ModeType {
    Stream,
    Block,
    Compressed,
}

impl FileType {
    #[allow(dead_code)]
    pub fn to_string(&self) -> String {
        match *self {
            FileType::ASCII => "A".to_string(),
            FileType::Binary => "I".to_string(),
        }
    }

    pub fn from_string(s: &str) -> Result<FileType, MessageParsingError> {
        match s {
            "A" => Ok(FileType::ASCII),
            "I" => Ok(FileType::Binary),
            _ => Err(MessageParsingError::UnknownFileType),
        }
    }
}

impl Default for FileType {
    fn default() -> FileType {
        FileType::ASCII
    }
}

impl ModeType {
    #[allow(dead_code)]
    pub fn to_string(&self) -> String {
        match *self {
            ModeType::Stream => "S".to_string(),
            ModeType::Block => "B".to_string(),
            ModeType::Compressed => "C".to_string(),
        }
    }

    pub fn from_string(s: &str) -> Result<ModeType, MessageParsingError> {
        match s {
            "S" => Ok(ModeType::Stream),
            "B" => Ok(ModeType::Block),
            "C" => Ok(ModeType::Compressed),
            _ => Err(MessageParsingError::UnknownModeType),
        }
    }
}

impl Default for ModeType {
    fn default() -> ModeType {
        ModeType::Stream
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_message_empty_message() {
        let result = FtpCommands::parse_message("");
        assert_eq!(result, Err(MessageParsingError::EmptyMessage));
    }

    #[test]
    fn test_parse_message_noop() {
        let result = FtpCommands::parse_message("NOOP");
        assert_eq!(result, Ok(FtpCommands::NOOP));
    }

    #[test]
    fn test_parse_message_quit() {
        let result = FtpCommands::parse_message("QUIT");
        assert_eq!(result, Ok(FtpCommands::QUIT));
    }

    #[test]
    fn test_parse_message_lacks_parameters() {
        let result = FtpCommands::parse_message("APPE");
        assert_eq!(result, Err(MessageParsingError::LacksParameters));
    }

    #[test]
    fn test_parse_message_cwd_with_parameters() {
        let result = FtpCommands::parse_message("CWD /");
        assert_eq!(result, Ok(FtpCommands::CWD(Some("/".to_string()))));
    }

    #[test]
    fn test_parse_message_mode_with_parameters() {
        let result = FtpCommands::parse_message("MODE S");
        assert_eq!(result, Ok(FtpCommands::MODE(ModeType::Stream)));
    }

    #[test]
    fn test_parse_message_failure_mode() {
        let message = "MODE M";
        let result = FtpCommands::parse_message(message);
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), MessageParsingError::UnknownModeType);
    }

    #[test]
    fn test_parse_message_failure_type() {
        let message = "TYPE ychgvh";
        let result = FtpCommands::parse_message(message);
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), MessageParsingError::UnknownFileType);
    }
}