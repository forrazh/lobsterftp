#![feature(let_chains)]
#![feature(is_some_and)]

mod client;
mod command_handlers;
mod commands;
mod errors;
mod server;
mod status_codes;
mod config;

use crate::server::ftp_server::FtpServerBuilder;
use chrono::{Datelike, Utc};
use clap::Parser;
use log::{error, info, LevelFilter};
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;
use log4rs::filter::threshold::ThresholdFilter;
use log4rs::Config;
use tokio::io;
use config::CliArgs;
use crate::config::AuthorizedUser;
use crate::server::Server;

#[tokio::main]
async fn main() -> io::Result<()> {
    let args = CliArgs::parse();
    let config = config::Config::read_config(&args.config_file).unwrap();

    init_logger(config.debug, config.get_debug_level());
    info!("Starting lobster_ftp\n");

    let authorized_users = match AuthorizedUser::read_config(&args.authorized_users_file) {
        Ok(users) => users,
        Err(e) => {
            error!("Could not read authorized users: {}\n", e);
            std::process::exit(1);
        }
    };
    let server = FtpServerBuilder::new()
        .root(config.server_root.clone())
        .address(config.address)
        .authorized_users(authorized_users)
        .port(config.port)
        .build();

    let dir_creation_result = tokio::fs::create_dir_all(config.server_root).await;

    match dir_creation_result {
        Ok(_) => info!("Created server root directory\n"),
        Err(e) => {
            if e.kind() == std::io::ErrorKind::AlreadyExists {
                info!("Server root directory already exists\n");
            } else {
                error!("Could not create server root directory: {}\n", e);
                std::process::exit(1);
            }
        },
    }

    server.run_server().await
}


/// Initializing the logger so it writes to a file in `/tmp/lobster_ftp/`
/// Only the `logging_level` might need to be changed during `dev`.
fn init_logger(debug_std: bool, logging_level: LevelFilter) {
    let now = Utc::now();
    let logger_error = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new("=== {l} === {f}:{L} :: {m}")))
        .build();
    let log_path = format!(
        "/tmp/lobster_ftp/{}-{:02}-{:02}.log",
        now.year(),
        now.month(),
        now.day()
    );
    let log_file = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{l}: {d(%Y-%m-%d %H:%M:%S)}\t- {f}:{L}\t:: {m}",
        )))
        .build(log_path)
        .unwrap();
    let std = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{l}: {d(%Y-%m-%d %H:%M:%S)}\t- {f}:{L}\t:: {m}",
        )))
        .build();
    let config_builder = Config::builder()
        .appender(Appender::builder().build("file_logger", Box::new(log_file)))
        .appender(Appender::builder().build("std", Box::new(std)))
        .appender(
            Appender::builder()
                .filter(Box::new(ThresholdFilter::new(LevelFilter::Error)))
                .build("error", Box::new(logger_error)),
        );

    let mut root_builder = Root::builder().appender("file_logger").appender("error");
    if debug_std {
        root_builder = root_builder.appender("std");
    }
    let config = config_builder
        .build(root_builder.build(logging_level))
        .unwrap();

    let _ = log4rs::init_config(config).unwrap();
}
