pub mod ftp_server;
mod mock_server;

use async_trait::async_trait;
use log::debug;
use tokio::io;
use tokio::net::{TcpListener, TcpStream};

const PORT: u16 = 21;

#[async_trait]
pub trait Server {
    async fn init_tcp_listener(&self) -> io::Result<TcpListener>;
    async fn handle_connection(&self, socket: TcpStream);
    fn can_run(&self) -> bool {
        true
    }
    async fn run_server(&self) -> io::Result<()> {
        let listener = self.init_tcp_listener().await?;
        while self.can_run() {
            debug!("Waiting for new connection\n");
            let (socket, _sock_addr) = listener.accept().await.unwrap();
            self.handle_connection(socket).await;
        }
        Ok(())
    }
}
