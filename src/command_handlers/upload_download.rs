pub mod list;
pub mod mkd;
pub mod retr;
pub mod stor;
pub mod rename;
pub mod delete;
pub mod thumb;

use crate::command_handlers::CommandResult;
use crate::errors::command_errors::CommandError;
use async_trait::async_trait;
use tokio::fs::File;
use tokio::io;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

const BUFFER_SIZE: usize = 4096;

async fn read_from_socket_and_write_to_file(socket: &mut TcpStream, file: &mut File) -> io::Result<usize> {
    let mut buffer = [0; BUFFER_SIZE];

    let n = socket.read(&mut buffer).await?;
    if n == 0 {
        return Ok(0);
    }
    file.write_all(&buffer[..n]).await?;
    Ok(n)
}

async fn read_from_file_and_write_to_socket(socket: &mut TcpStream, file: &mut File) -> io::Result<usize> {
    let mut buffer = [0; BUFFER_SIZE];

    let n = file.read(&mut buffer).await?;
    if n == 0 {
        return Ok(0);
    }
    socket.write_all(&buffer[..n]).await?;
    Ok(n)
}

#[async_trait]
pub trait FileOverSocketCommand: CommandWithPath {
    async fn handle(
        &self,
        mut socket: TcpStream,
        path: &str,
    ) -> Result<CommandResult, CommandError>;
}

pub trait CommandWithPath {
    fn get_root(&self) -> &str;
    fn get_current_path(&self) -> &str;

    fn get_full_path(&self, new_path: &str) -> String {
        let new_path = if new_path.starts_with("~/") {
            &new_path[1..]
        } else {
            new_path
        };

        let path = if new_path.starts_with('/') {
            let split_path = new_path.split('/');
            let mut n_path = Vec::new();
            for x in split_path {
                if x == ".." {
                    n_path.pop();
                } else if x.is_empty() || x == "." {
                    continue;
                } else {
                    n_path.push(x);
                }
            }
            let mut path = String::new();
            for item in n_path {
                path.push('/');
                path.push_str(item);
            }
            if path.is_empty() {
                path = String::from("/");
            }
            path
        } else {
            let split_new_path = new_path.split('/');
            let mut path = self.get_current_path()
                .split('/')
                .filter(|x| !x.is_empty())
                .collect::<Vec<&str>>(); // empty means root
            let mut new_path = String::from("/");

            for x in split_new_path {
                if x == ".." {
                    path.pop();
                } else if x == "." || x.is_empty() {
                    continue;
                } else {
                    path.push(x);
                }
            }

            for x in path {
                new_path.push_str(x);
                new_path.push('/');
            }

            new_path[..new_path.len() - 1].to_string()
        };

        let mut full_path = self.get_root().to_string();

        if !path.is_empty() && !path.starts_with('/') {
            if !full_path.ends_with('/') {
                full_path.push('/');
            }
            full_path.push_str(self.get_current_path());

            if !full_path.ends_with('/') {
                full_path.push('/');
            }
        }
        full_path.push_str(&path);
        full_path
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::command_handlers::upload_download::list::List;
    use crate::command_handlers::upload_download::mkd::Mkd;
    use crate::command_handlers::upload_download::retr::Retr;
    use crate::command_handlers::upload_download::stor::{Stor, Stou};
    use std::path::Path;
    use tokio::fs::{create_dir, remove_dir, remove_file, File};
    use tokio::io::{AsyncReadExt, AsyncWriteExt};
    use tokio::net::TcpListener;

    #[tokio::test]
    async fn test_stor() {
        let root = "/tmp";
        let path = "/test_stor.txt";
        let stor = Stor::new(root.to_string(), "/".to_string());

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = stor.handle(socket, path).await;
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), CommandResult::FileUploaded);
        });

        let mut socket = TcpStream::connect(addr).await.unwrap();
        socket.write_all(b"hello world").await.unwrap();
        socket.shutdown().await.unwrap();
        let _ = t.await;

        let path = Path::new("/tmp/test_stor.txt");
        let mut contents = String::new();

        let res_file = File::open(path).await;

        let mut file = match res_file {
            Ok(f) => f,
            Err(e) => {
                panic!("Error while opening file: {}", e);
            }
        };
        file.read_to_string(&mut contents).await.unwrap();
        remove_file(path).await.unwrap();

        assert_eq!(contents, "hello world");
    }

    #[tokio::test]
    async fn test_stor_image() {
        let root = "/tmp";
        let path = "/test_stor_image.jpg";
        let stor = Stor::new(root.to_string(), "/".to_string());

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = stor.handle(socket, path).await;
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), CommandResult::FileUploaded);
        });

        let mut file = File::open("res/tests/test_stor_img.png").await.unwrap();
        let mut contents = Vec::new();
        file.read_to_end(&mut contents).await.unwrap();

        let mut socket = TcpStream::connect(addr).await.unwrap();
        socket.write_all(&contents).await.unwrap();
        socket.shutdown().await.unwrap();
        let _ = t.await;

        let path = Path::new("/tmp/test_stor_image.jpg");
        let mut file_downloaded = Vec::new();

        let res_file = File::open(path).await;

        let mut file = match res_file {
            Ok(f) => f,
            Err(e) => {
                panic!("Error while opening file: {}", e);
            }
        };
        file.read_to_end(&mut file_downloaded).await.unwrap();
        remove_file(path).await.unwrap();

        assert_eq!(contents, file_downloaded);
    }

    #[tokio::test]
    async fn test_stou() {
        let root = "/tmp";
        let path = "/test_stou_write.txt";
        let stou = Stou::new(root.to_string(), "/".to_string());

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = stou.handle(socket, path).await;
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), CommandResult::FileUploaded);
        });

        let mut socket = TcpStream::connect(addr).await.unwrap();
        socket.write_all(b"hello world").await.unwrap();
        socket.shutdown().await.unwrap();
        let _ = t.await;

        let path = Path::new("/tmp/test_stou_write.txt");
        let mut contents = String::new();

        let res_file = File::open(path).await;

        let mut file = match res_file {
            Ok(f) => f,
            Err(e) => {
                panic!("Error while opening file: {}", e);
            }
        };
        file.read_to_string(&mut contents).await.unwrap();
        remove_file(path).await.unwrap();

        assert_eq!(contents, "hello world");
    }

    #[tokio::test]
    async fn test_stou_file_already_exists() {
        let root = "/tmp";
        let path = "/test_stou.txt";
        let stou = Stou::new(root.to_string(), "/".to_string());

        let const_path = Path::new("/tmp/test_stou.txt");

        let _res_file = File::create(const_path).await;

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = stou.handle(socket, path).await;
            assert!(result.is_err());
            assert_eq!(result.unwrap_err(), CommandError::FileAlreadyExists);
        });

        let mut socket = TcpStream::connect(addr).await.unwrap();
        socket.write_all(b"hello world").await.unwrap();
        socket.shutdown().await.unwrap();
        let _ = t.await;

        // assert_eq!(contents, "hello world");
    }

    #[tokio::test]
    async fn test_list() {
        let root = "/tmp";
        let path = "/test_list_dir";
        let full_path = format!("{}/{}", root, path);
        let list = List::new(root.to_string(), "/".to_string());

        create_dir(&full_path).await.unwrap();
        let mut test_file = File::create(format!("{}/test_list_file.txt", &full_path))
            .await
            .unwrap();
        test_file.write_all(b"hello world").await.unwrap();
        test_file.flush().await.unwrap();
        test_file.sync_all().await.unwrap();

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = list.handle(socket, path).await;
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), CommandResult::FileUploaded);
        });

        let mut socket = TcpStream::connect(addr).await.unwrap();
        let mut contents = String::new();
        socket.read_to_string(&mut contents).await.unwrap();
        socket.shutdown().await.unwrap();
        let _ = t.await;

        assert!(contents.contains("test_list_file.txt"));
        assert!(contents.starts_with('-'));

        remove_file(format!("{}/test_list_file.txt", full_path))
            .await
            .unwrap();
        remove_dir(full_path).await.unwrap();
    }

    #[tokio::test]
    async fn test_mkd() {
        let root = "/tmp";
        let path = "/test_mkd_dir";
        let second_path = "/test_mkd_dir/test_inner_mkd_dir";
        let mkd = Mkd::new(root.to_string(), "/".to_string());

        let result = mkd.handle(path).await;
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), CommandResult::DirectoryCreated);

        let result = mkd.handle(second_path).await;
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), CommandResult::DirectoryCreated);

        let path = Path::new("/tmp/test_mkd_dir");
        let second_path = Path::new("/tmp/test_mkd_dir/test_inner_mkd_dir");
        assert!(path.exists());
        assert!(second_path.exists());
        remove_dir(second_path).await.unwrap();
        remove_dir(path).await.unwrap();
    }

    #[tokio::test]
    async fn test_retr() {
        let root = "/tmp";
        let path = "/test_retr.txt";
        let retr = Retr::new(root.to_string(), "/".to_string());
        let full_path = retr.get_full_path(path);
        let mut file = File::create(full_path.clone()).await.unwrap();
        file.write_all(b"hello world").await.unwrap();

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = retr.handle(socket, path).await;
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), CommandResult::FileUploaded);
        });

        let mut socket = TcpStream::connect(addr).await.unwrap();
        let mut contents = vec![];
        socket.read_to_end(&mut contents).await.unwrap();
        socket.shutdown().await.unwrap();
        let _ = t.await;

        assert_eq!(contents, b"hello world");
        remove_file(full_path).await.unwrap();
    }

    #[tokio::test]
    async fn test_retr_image() {
        let root = "res";
        let path = "test_retr_img.png";
        let retr = Retr::new(root.to_string(), "tests".to_string());
        let full_path = retr.get_full_path(path);

        let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
        let addr = listener.local_addr().unwrap();
        let t = tokio::spawn(async move {
            let (socket, _) = listener.accept().await.unwrap();
            let result = retr.handle(socket, path).await;
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), CommandResult::FileUploaded);
        });

        let mut file = File::open("res/tests/test_retr_img.png").await.unwrap();
        let mut contents = Vec::new();
        file.read_to_end(&mut contents).await.unwrap();

        let mut socket = TcpStream::connect(addr).await.unwrap();
        loop {
            let mut buffer = [0; 1024];
            let bytes_read = socket.read(&mut buffer).await.unwrap();
            if bytes_read == 0 {
                break;
            }
        }
        socket.shutdown().await.unwrap();
        let _ = t.await;

        let mut contents_downloaded = Vec::new();
        File::open(&full_path)
            .await
            .unwrap()
            .read_to_end(&mut contents_downloaded)
            .await
            .unwrap();

        assert_eq!(contents, contents_downloaded);
    }
}
