use crate::command_handlers::{CommandResult, FileCommandHandler};
use crate::errors::command_errors::CommandError;
use log::debug;
use std::path::Path;
use crate::command_handlers::upload_download::CommandWithPath;

pub struct CWD {
    root: String,
    old_path: String,
}

impl CWD {
    pub fn new(root: String, old_path: String) -> CWD {
        CWD { root, old_path }
    }
}

impl CommandWithPath for CWD {
    fn get_root(&self) -> &str {
        self.root.as_str()
    }

    fn get_current_path(&self) -> &str {
        self.old_path.as_str()
    }
}

impl FileCommandHandler for CWD {
    fn handle(&self, new_path: &str) -> Result<CommandResult, CommandError> {
        let mut path = self.get_full_path(new_path);
        let as_path = Path::new(&path);
        if !as_path.exists() {
            debug!("Path `{}` does not exist\n", path);
            return Err(CommandError::FileNotFound);
        }

        if !path.ends_with('/')
            && as_path.is_dir() {
            path.push('/');
        }

        debug!("New path: {}\n", path);
        let final_path = &path[self.root.len()..];
        Ok(CommandResult::ChangedDirectory { new_path: final_path.to_string() })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cwd_handle_with_existing_path() {
        let root = ".".to_string();
        let old_path = "/data".to_string();
        let cwd = CWD::new(root, old_path);
        let result = cwd.handle("/src");
        let expected = Ok(CommandResult::ChangedDirectory {
            new_path: "/src/".to_string(),
        });
        assert_eq!(result, expected);
    }

    #[test]
    fn test_cwd_handle_with_nonexistent_path() {
        let root = ".".to_string();
        let old_path = "/".to_string();
        let cwd = CWD::new(root, old_path);
        let result = cwd.handle("/non_existent");
        let expected = Err(CommandError::FileNotFound);
        assert_eq!(result, expected);
    }

    #[test]
    fn test_cwd_handle_with_relative_path() {
        let root = ".".to_string();
        let old_path = "/src".to_string();
        let cwd = CWD::new(root, old_path);
        let result = cwd.handle("command_handlers");
        let expected = Ok(CommandResult::ChangedDirectory {
            new_path: "/src/command_handlers/".to_string(),
        });
        assert_eq!(result, expected);
    }

    #[test]
    fn test_cwd_to_previous() {
        let root = ".".to_string();
        let old_path = "/src".to_string();
        let cwd = CWD::new(root, old_path);
        let result = cwd.handle("..");
        let expected = Ok(CommandResult::ChangedDirectory {
            new_path: "/".to_string(),
        });
        assert_eq!(result, expected);
    }
}
