use crate::command_handlers::upload_download::CommandWithPath;
use crate::command_handlers::CommandResult;
use crate::errors::command_errors::CommandError;
use log::{error, debug};
use std::path::Path;
use tokio::fs::create_dir;

pub struct Mkd {
    root: String,
    curr_path: String,
}

impl Mkd {
    pub fn new(root: String, curr_path: String) -> Self {
        Mkd { root, curr_path }
    }

    pub async fn handle(&self, path: &str) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);

        if Path::new(&full_path).exists() {
            return Err(CommandError::FileAlreadyExists);
        }

        create_dir(full_path)
            .await
            .map_err(|e| {
                error!("Error occured while creating the directory: {}\n", e);
                CommandError::FileNotFound
            })?;

            debug!("Created directory");

        Ok(CommandResult::DirectoryCreated)
    }
}

impl CommandWithPath for Mkd {
    fn get_root(&self) -> &str {
        &self.root
    }

    fn get_current_path(&self) -> &str {
        &self.curr_path
    }
}
