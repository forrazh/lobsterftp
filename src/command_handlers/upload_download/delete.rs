use std::path::Path;
use crate::command_handlers::CommandResult;
use crate::command_handlers::upload_download::CommandWithPath;
use crate::errors::command_errors::CommandError;

pub struct Delete {
    root: String,
    curr_path: String,
}

impl Delete {
    pub fn new(root: String, curr_path: String) -> Self {
        Delete { root, curr_path }
    }

    pub async fn delete_file(&self, path: &str) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);
        let as_path = Path::new(&full_path);

        if as_path.exists()  {
            if as_path.is_dir() {
                return Err(CommandError::IsADirectoryNotAFile);
            }
            tokio::fs::remove_file(full_path).await.map_err(|_| CommandError::FileNotFound)?;
            Ok(CommandResult::FileDeleted)
        } else {
            Err(CommandError::FileNotFound)
        }
    }

    pub async fn delete_directory(&self, path: &str) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);
        let as_path = Path::new(&full_path);

        if as_path.exists()  {
            if as_path.is_file() {
                return Err(CommandError::NotADirectory);
            }
            if as_path.read_dir().unwrap().count() > 0 {
                return Err(CommandError::DirectoryNotEmpty);
            }
            tokio::fs::remove_dir(full_path).await.map_err(|_| CommandError::FileNotFound)?;
            Ok(CommandResult::DirectoryDeleted)
        } else {
            Err(CommandError::FileNotFound)
        }
    }

    pub async fn delete_directory_and_contents(&self, path: &str) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);
        let as_path = Path::new(&full_path);

        if as_path.exists()  {
            if as_path.is_file() {
                return Err(CommandError::NotADirectory);
            }
            tokio::fs::remove_dir_all(full_path).await.map_err(|_| CommandError::FileNotFound)?;
            Ok(CommandResult::DirectoryDeleted)
        } else {
            Err(CommandError::FileNotFound)
        }
    }
}

impl CommandWithPath for Delete {
    fn get_root(&self) -> &str {
        &self.root
    }

    fn get_current_path(&self) -> &str {
        &self.curr_path
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;
    use tokio::fs::{create_dir, File, remove_dir_all};
    use tokio::io::AsyncWriteExt;
    use crate::command_handlers::CommandResult;
    use crate::command_handlers::upload_download::delete::Delete;
    use crate::errors::command_errors::CommandError;

    #[tokio::test]
    async fn test_delete_file() {
        let root = "/tmp";
        let path = "test_delete_file.txt";
        let full_path = format!("{}/{}", root, path);
        let delete = Delete::new(root.to_string(), "/".to_string());

        let mut file = File::create(full_path.clone()).await.unwrap();
        file.write_all(b"test contents").await.unwrap();

        let result = delete.delete_file(path).await;
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), CommandResult::FileDeleted);
        assert!(!Path::new(&full_path).exists());
    }

    #[tokio::test]
    async fn test_delete_file_not_found() {
        let root = "/tmp";
        let path = "test_delete_file_not_found.txt";
        let delete = Delete::new(root.to_string(), "/".to_string());

        let result = delete.delete_file(path).await;
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), CommandError::FileNotFound);
    }

    #[tokio::test]
    async fn test_delete_directory() {
        let root = "/tmp";
        let path = "test_delete_directory";
        let full_path = format!("{}/{}", root, path);
        let delete = Delete::new(root.to_string(), "/".to_string());

        create_dir(full_path.clone()).await.unwrap();
        let result = delete.delete_directory(path).await;
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), CommandResult::DirectoryDeleted);
        assert!(!Path::new(&full_path).exists());
    }

    #[tokio::test]
    async fn test_delete_directory_not_empty() {
        let root = "/tmp";
        let path = "test_delete_directory_not_empty";
        let full_path = format!("{}/{}", root, path);
        let delete = Delete::new(root.to_string(), "/".to_string());

        create_dir(full_path.clone()).await.unwrap();
        let file_path = format!("{}/{}", full_path, "file.txt");
        let _file = File::create(file_path).await.unwrap();

        let result = delete.delete_directory(path).await;
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), CommandError::DirectoryNotEmpty);

        remove_dir_all(full_path).await.unwrap();
    }
    #[tokio::test]
    async fn test_delete_directory_not_found() {
        let root = "/tmp";
        let curr_path = "/".to_string();
        let delete = Delete::new(root.to_string(), curr_path);

        let result = delete.delete_directory("non_existent_directory").await;
        assert!(result.is_err());
        assert_eq!(result.err().unwrap(), CommandError::FileNotFound);
    }

    #[tokio::test]
    async fn test_delete_directory_and_contents_recursive() {
        let root = "/tmp";
        let curr_path = "/".to_string();
        let delete = Delete::new(root.to_string(), curr_path);

        let test_dir = Path::new("/tmp/test_delete_directory_and_contents_recursive");
        tokio::fs::create_dir(test_dir).await.unwrap();

        let test_file = Path::new("/tmp/test_delete_directory_and_contents_recursive/test_file.txt");
        let mut file = File::create(test_file).await.unwrap();
        file.write_all(b"test content").await.unwrap();

        let result = delete.delete_directory_and_contents("/test_delete_directory_and_contents_recursive").await;
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), CommandResult::DirectoryDeleted);

        let dir_exists = test_dir.exists();
        let file_exists = test_file.exists();
        assert!(!dir_exists && !file_exists);
    }
}