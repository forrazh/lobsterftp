use crate::command_handlers::upload_download::{FileOverSocketCommand, CommandWithPath, read_from_socket_and_write_to_file};
use crate::command_handlers::CommandResult;
use crate::errors::command_errors::CommandError;
use async_trait::async_trait;
use log::{error, info, warn};
use std::path::Path;
use tokio::fs::File;
use tokio::net::TcpStream;
use tokio::io::AsyncWriteExt;

pub struct Stor {
    root: String,
    curr_path: String,
}

pub struct Stou {
    root: String,
    curr_path: String,
}

impl Stor {
    pub fn new(root: String, curr_path: String) -> Self {
        Stor { root, curr_path }
    }
}

impl Stou {
    pub fn new(root: String, curr_path: String) -> Self {
        Stou { root, curr_path }
    }
}

#[async_trait]
impl FileOverSocketCommand for Stor {
    async fn handle(
        &self,
        mut socket: TcpStream,
        path: &str,
    ) -> Result<CommandResult, CommandError> {
        let mut file = File::create(self.get_full_path(path)).await.map_err(|e| {
            warn!("PATH: {}\n", self.get_full_path(path));
            error!("Error while creating file: {}\n", e);
            CommandError::DownloadFailed
        })?;

        loop {
            match read_from_socket_and_write_to_file(&mut socket, &mut file).await {
                Ok(0) => break,
                Ok(_) => continue,
                Err(e) => {
                    error!("Error while reading and writing: {}\n", e);
                    return Err(CommandError::DownloadFailed);
                }
            }
        }
        info!("Finished downloading {}/{}.", self.root, path);
        socket.shutdown().await.map_err(|_| CommandError::Ded)?;

        Ok(CommandResult::FileUploaded)
    }
}

#[async_trait]
impl FileOverSocketCommand for Stou {
    async fn handle(
        &self,
        socket: TcpStream,
        path: &str,
    ) -> Result<CommandResult, CommandError> {
        let p = self.get_full_path(path);
        if Path::new(&p).exists() {
            return Err(CommandError::FileAlreadyExists);
        }

        Stor::new(self.root.clone(), self.curr_path.clone())
            .handle(socket, path)
            .await
    }
}

impl CommandWithPath for Stor {
    fn get_root(&self) -> &str {
        &self.root
    }

    fn get_current_path(&self) -> &str {
        &self.curr_path
    }
}

impl CommandWithPath for Stou {
    fn get_root(&self) -> &str {
        &self.root
    }

    fn get_current_path(&self) -> &str {
        &self.curr_path
    }
}
