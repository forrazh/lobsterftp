use crate::command_handlers::upload_download::{
    FileOverSocketCommand, CommandWithPath
};
use crate::command_handlers::CommandResult;
use crate::errors::command_errors::CommandError;
use async_trait::async_trait;
use chrono::{DateTime, Datelike, Utc};
use std::os::unix::fs::{MetadataExt, PermissionsExt};
use log::info;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;

pub struct List {
    root: String,
    curr_path: String,
}

impl List {
    pub fn new(root: String, curr_path: String) -> Self {
        List { root, curr_path }
    }

    const fn get_month(month: u32) -> &'static str {
        match month {
            2 => "Feb",
            3 => "Mar",
            4 => "Apr",
            5 => "May",
            6 => "Jun",
            7 => "Jul",
            8 => "Aug",
            9 => "Sep",
            10 => "Oct",
            11 => "Nov",
            12 => "Dec",
            _ => "Jan",
        }
    }

    fn get_rights(rights: u32) -> String {
        let mut result = String::new();
        if rights & 0o400 != 0 {
            result.push('r');
        } else {
            result.push('-');
        }
        if rights & 0o200 != 0 {
            result.push('w');
        } else {
            result.push('-');
        }
        if rights & 0o100 != 0 {
            result.push('x');
        } else {
            result.push('-');
        }
        if rights & 0o040 != 0 {
            result.push('r');
        } else {
            result.push('-');
        }
        if rights & 0o020 != 0 {
            result.push('w');
        } else {
            result.push('-');
        }
        if rights & 0o010 != 0 {
            result.push('x');
        } else {
            result.push('-');
        }
        if rights & 0o004 != 0 {
            result.push('r');
        } else {
            result.push('-');
        }
        if rights & 0o002 != 0 {
            result.push('w');
        } else {
            result.push('-');
        }
        if rights & 0o001 != 0 {
            result.push('x');
        } else {
            result.push('-');
        }
        result
    }
}

#[async_trait]
impl FileOverSocketCommand for List {
    async fn handle(
        &self,
        mut socket: TcpStream,
        path: &str,
    ) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);

        let mut dir_content = tokio::fs::read_dir(full_path)
            .await
            .map_err(|_| CommandError::FileNotFound)?;

        while let Ok(Some(entry)) = dir_content.next_entry().await {
            let path = entry.path();
            let file_name = path.file_name().unwrap().to_str().unwrap();
            let metadata = entry
                .metadata()
                .await
                .map_err(|_| CommandError::FileNotFound)?;
            let is_dir = metadata.is_dir();
            let rights = metadata.permissions().mode();
            let size = metadata.len();
            let links = metadata.nlink();
            let user = metadata.uid();
            let group = metadata.gid();
            let modified = metadata.modified().unwrap();
            let chrono = DateTime::<Utc>::from(modified);
            let month = List::get_month(chrono.month());
            let day = chrono.day();
            let year_or_time = if chrono.year() == Utc::now().year() {
                chrono.format("%H:%M").to_string()
            } else {
                chrono.format("%Y").to_string()
            };

            let file_type = if is_dir { "d" } else { "-" };
            let line = format!(
                "{}{} {}\t{}\t{}\t{}\t{} {}\t{}\t{}\r\n",
                file_type,
                List::get_rights(rights),
                links,
                group,
                user,
                size,
                month,
                day,
                year_or_time,
                file_name
            );
            socket.write_all(line.as_bytes()).await.unwrap();
        }

        info!("Finished downloading {}/{}.", self.root, path);
        socket.shutdown().await.map_err(|_| CommandError::Ded)?;

        Ok(CommandResult::FileUploaded)
    }
}

impl CommandWithPath for List {
    fn get_root(&self) -> &str {
        &self.root
    }

    fn get_current_path(&self) -> &str {
        &self.curr_path
    }
}

#[cfg(test)]
mod test {
    use crate::command_handlers::upload_download::list::List;

    #[test]
    fn test_get_month() {
        assert_eq!(List::get_month(2), "Feb");
        assert_eq!(List::get_month(3), "Mar");
        assert_eq!(List::get_month(12), "Dec");
        assert_eq!(List::get_month(0), "Jan");
    }

    #[test]
    fn test_get_rights() {
        assert_eq!(List::get_rights(0o400), "r--------");
        assert_eq!(List::get_rights(0o644), "rw-r--r--");
        assert_eq!(List::get_rights(0o755), "rwxr-xr-x");
        assert_eq!(List::get_rights(0o000), "---------");
    }
}
