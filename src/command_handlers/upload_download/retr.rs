use crate::command_handlers::upload_download::{FileOverSocketCommand, CommandWithPath, read_from_file_and_write_to_socket};
use crate::command_handlers::CommandResult;
use crate::errors::command_errors::CommandError;
use async_trait::async_trait;
use log::{error, info};
use tokio::io::AsyncWriteExt;
use tokio::fs::File;
use tokio::net::TcpStream;

pub struct Retr {
    root: String,
    curr_path: String,
}

impl Retr {
    pub fn new(root: String, curr_path: String) -> Self {
        Retr { root, curr_path }
    }
}

#[async_trait]
impl FileOverSocketCommand for Retr {
    async fn handle(
        &self,
        mut socket: TcpStream,
        path: &str,
    ) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);
        let mut file = File::open(full_path).await.map_err(|e| {
            error!("Error occurred while opening the file : {}", e);
            CommandError::FileNotFound
        })?;

        loop {
            match read_from_file_and_write_to_socket(&mut socket, &mut file).await {
                Ok(0) => break,
                Ok(_) => continue,
                Err(e) => {
                    error!("Error while reading and writing: {}\n", e);
                    return Err(CommandError::UploadFailed);
                }
            }
        }
        info!("Finished downloading {}/{}.", self.root, path);
        socket.shutdown().await.map_err(|e| {
            error!("Error occurred while closing the socket : {}", e);
            CommandError::Ded
        })?;

        Ok(CommandResult::FileUploaded)
    }
}

impl CommandWithPath for Retr {
    fn get_root(&self) -> &str {
        &self.root
    }

    fn get_current_path(&self) -> &str {
        &self.curr_path
    }
}
