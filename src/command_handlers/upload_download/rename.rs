use crate::command_handlers::upload_download::CommandWithPath;

pub struct Renamer {
    root: String,
    curr_path: String,
}

impl Renamer {
    pub fn new(root: String, curr_path: String) -> Self {
        Self { root, curr_path }
    }
}

impl CommandWithPath for Renamer {
    fn get_root(&self) -> &str {
        self.root.as_str()
    }

    fn get_current_path(&self) -> &str {
        self.curr_path.as_str()
    }
}
