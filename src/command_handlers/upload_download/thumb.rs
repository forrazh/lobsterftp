use std::io::BufReader;
use log::{error, debug};
use tokio::fs::File;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use crate::command_handlers::CommandResult;
use crate::command_handlers::upload_download::{CommandWithPath, FileOverSocketCommand};
use crate::errors::command_errors::CommandError;
use async_trait::async_trait;

pub struct Thumb {
    root: String,
    current_path: String,
}

impl CommandWithPath for Thumb {
    fn get_current_path(&self) -> &str {
        &self.current_path
    }

    fn get_root(&self) -> &str {
        &self.root
    }
}

impl Thumb {
    pub fn new(root: String, current_path: String) -> Self {
        Self {
            root,
            current_path,
        }
    }
}

#[async_trait]
impl FileOverSocketCommand for Thumb {
    async fn handle(&self, mut socket: TcpStream, path: &str) -> Result<CommandResult, CommandError> {
        let full_path = self.get_full_path(path);
        let file = File::open(full_path).await.map_err(|e| {
            error!("Error occurred while opening the file : {}", e);
            CommandError::FileNotFound
        })?;


        let thumbnail = image::io::Reader::new(BufReader::new(file.into_std().await))
            .with_guessed_format().map_err(|e| {
                error!("Error occurred while guessing the format : {}", e);
                CommandError::UploadFailed
            })?
            .decode()
            .unwrap()
            .thumbnail(128, 128);
        let thumbnail_buffer = thumbnail.as_bytes();

        socket.write_all(thumbnail_buffer).await.map_err(|e| {
            error!("Error occurred while writing to the socket : {}", e);
            CommandError::UploadFailed
        })?;
        
        debug!("Sent thumbnail...");

        socket.shutdown().await.map_err(|e| {
            error!("Error occurred while closing the socket : {}", e);
            CommandError::Ded
        })?;

        Ok(CommandResult::FileUploaded)
    }
}