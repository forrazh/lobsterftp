use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, PartialEq)]
pub enum CommandError {
    LoginFirst(String),
    Ded,
    UploadFailed,
    DownloadFailed,
    FileNotFound,
    FileAlreadyExists,
    NotADirectory,
    IsADirectoryNotAFile,
    DirectoryNotEmpty
}

impl Display for CommandError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CommandError::LoginFirst(s) => write!(f, "Login first: {}", s),
            CommandError::Ded => write!(f, "Ded"),
            CommandError::UploadFailed => write!(f, "Upload failed"),
            CommandError::DownloadFailed => write!(f, "Download failed"),
            CommandError::FileNotFound => write!(f, "File not found"),
            CommandError::FileAlreadyExists => write!(f, "File already exists (UNIQUE)"),
            CommandError::NotADirectory => write!(f, "Not a directory"),
            CommandError::IsADirectoryNotAFile => write!(f, "Is a directory, not a file"),
            CommandError::DirectoryNotEmpty => write!(f, "Directory not empty"),
        }
    }
}

impl std::error::Error for CommandError {}
