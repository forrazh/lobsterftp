use std::fmt::{Debug, Display, Formatter};

/// The different error cases that can happen while parsing a message.
/// Doesn't handle a FILE NOT FOUND error, because this is not a parsing error.
#[derive(Debug, PartialEq)]
pub enum MessageParsingError {
    CommandDoesNotExist,
    EmptyMessage,
    LacksParameters,
    UnknownModeType,
    UnknownFileType,
}

impl Display for MessageParsingError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MessageParsingError::CommandDoesNotExist => write!(
                f,
                "This command does not exist, has not been understood or contains an error."
            ),
            MessageParsingError::EmptyMessage => {
                write!(f, "This message should not be empty here.")
            }
            MessageParsingError::LacksParameters => write!(f, "This command needs a parameter."),
            MessageParsingError::UnknownModeType => write!(f, "This mode type is unknown."),
            MessageParsingError::UnknownFileType => write!(f, "This file type is unknown."),
        }
    }
}

impl std::error::Error for MessageParsingError {}
