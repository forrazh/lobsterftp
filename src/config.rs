use clap::Parser;
use log::{error, LevelFilter};
use serde::Deserialize;

/// An opinionated yet simple FTP server.
/// Read the README.md for more information.
///
/// Author: Hugo Forraz <hugo@forraz.com>
/// Version: 1.0.5
///
/// Realized as part of the Distributed Systems course at the University of Lille.
#[derive(Debug, Parser)]
#[command(author, version, about)]
pub struct CliArgs {
    /// Path for the file containing the server's configuration
    pub config_file: String,
    /// Path for the file containing the server's authorized users
    pub authorized_users_file: String,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub server_root: String,
    pub address: String,
    pub port: u16,
    pub debug_level: u8,
    pub debug: bool,

}

#[derive(Debug, Deserialize)]
pub struct AuthorizedUser {
    pub name: String,
    pub pass: String,
}

#[derive(Debug, Deserialize)]
struct AuthorizedDeserializer {
    authorized: Vec<AuthorizedUser>,
}

impl Config {
    pub(crate) fn read_config(path: &str) -> Result<Config, toml::de::Error> {
        let config_str = match std::fs::read_to_string(path) {
            Ok(s) => s,
            Err(e) => {
                error!("Could not read config: {}\n", e);
                std::process::exit(1);
            }
        };
        let config_deserializer = toml::from_str::<Config>(config_str.as_str())?;
        Ok(config_deserializer)
    }

    pub fn get_debug_level(&self) -> LevelFilter {
        match self.debug_level {
            0 => LevelFilter::Trace,
            1 => LevelFilter::Debug,
            2 => LevelFilter::Info,
            3 => LevelFilter::Warn,
            4 => LevelFilter::Error,
            _ => LevelFilter::Off,
        }
    }
}


impl AuthorizedUser {
    pub(crate) fn read_config(path: &str) -> Result<Vec<AuthorizedUser>, toml::de::Error> {
        let authorized_users_str = match std::fs::read_to_string(path) {
            Ok(s) => s,
            Err(e) => {
                error!("Could not read authorized users: {}\n", e);
                std::process::exit(1);
            }
        };
        let authorized_deserializer =
            toml::from_str::<AuthorizedDeserializer>(authorized_users_str.as_str())?;
        Ok(authorized_deserializer.authorized)
    }
}
