use std::env;

/// The status codes that are used in the FTP protocol.
/// The status codes are defined in [RFC 959](https://tools.ietf.org/html/rfc959) for most.
#[allow(dead_code)]
#[derive(Debug, PartialEq, Clone)]
pub enum FtpStatus {
    // 100 Series - The requested action is being initiated, expect another reply before proceeding with a new command.
    /// Restart marker reply.
    /// In this case, the text is exact and not left to the particular implementation; it must read:
    /// MARK yyyy = mmmm
    RestartMarkerReplay,
    /// Service ready in nnn minutes.
    ServiceReadyInNNN,
    /// Data connection already open; transfer starting.
    DataConnectionAlreadyOpen,
    /// File status okay; about to open data connection.
    FileStatusOk,
    // 200 Series - The requested action has been successfully completed.
    /// Sent when the user asks for a command that exists but which has not been implemented.
    CommandNotImplementedUnused,
    Features,
    /// System status, or system help reply.
    SystemStatus(String),
    DirectoryStatus,
    FileStatus,
    HelpMessage,
    NameSystemType,
    ServiceReady,
    ServiceClosingControlConnection,
    DataConnectionOpen,
    ClosingDataConnection,
    EnteringPassiveMode(String),
    EnteringLongPassiveMode,
    EnteringExtendedPassiveMode,
    UserLoggedIn,
    UserAlreadyLoggedIn,
    UserLoggedOut,
    LoggingOut,
    SecurityDataExchangeComplete,
    SecurityDataExchangeSuccessful,
    RequestedFileActionOk,
    PathnameCreated(String, String),
    // 300 Series - The command has been accepted, but the requested action is on hold, pending receipt of further information.
    UserNameOkay,
    NeedAccountForLogin,
    SecurityMechanismIsAccepted,
    RequestedFileActionPendingFurtherInformation,
    // 400 Series - The command was not accepted and the requested action did not take place, but the error condition is temporary and the action may be requested again.
    ServiceNotAvailable,
    CantOpenDataConnection,
    ConnectionClosed,
    InvalidUserNameOrPassword,
    RequestedHostUnavailable,
    RequestedFileActionNotTaken,
    RequestedActionAborted,
    RequestedActionNotTaken,
    // 500 Series - Syntax error, command unrecognized and the requested action did not take place. This may include errors such as command line too long.
    SyntaxErrorInParametersOrArguments,
    CommandNotImplementedUnknown,
    BadCommandSequence(Option<String>),
    CommandNotImplementedForThatParameter,
    NotLoggedIn,
    NeedAccountForStoringFiles,
    RequestDeniedForPolicyReasons,
    RequestedActionNotTakenFileUnavailable,
    RequestedActionAbortedPageTypeUnknown,
    RequestedFileActionAbortedExceededStorageAllocation,
    RequestedActionNotTakenFileBusy,
    // 600 Series - These replies refer to confidentiality and integrity features.
    IntegrityProtectedReply,
    ConfidentialityAndIntegrityProtectedReply,
    ConfidentialityProtectedReply,
    // 1000 Series - Common Winsock Error Codes
    ConnectionResetByPeer,
    CannotConnectToRemoteServer,
    CannotConnectToRemoteServerRefused,
    NoRouteToHost,
    DirectoryNotEmpty,
    TooManyUsers,
}

impl FtpStatus {
    pub fn as_string(&self) -> String {
        match self {
            FtpStatus::RestartMarkerReplay => "110 Restart Marker Replay, like a lobster catching the same wave twice\n".to_string(),
            FtpStatus::ServiceReadyInNNN => "120 Service ready in nnn minutes, like a lobster waiting for the right tide\n".to_string(),
            FtpStatus::DataConnectionAlreadyOpen => "125 Data Connection Already Open, like a lobster already settled in its rock\n".to_string(),
            FtpStatus::FileStatusOk => "150 File Status Okay, like a lobster feeling just fine after a satisfying meal.\n".to_string(),
            FtpStatus::CommandNotImplementedUnused => "202 Command not implemented, maybe a crab's trying to use a tool it doesn't understand\n".to_string(),
            FtpStatus::Features => "211-Features:\r\n PASV\r\n211 End\r\n".to_string(),
            FtpStatus::SystemStatus(appendix) => format!("211 System status, checking the currents before a big dive\r\n{}\r\n211 End of status\r\n", appendix).to_string(),
            FtpStatus::DirectoryStatus => "212 Directory status, exploring the mysteries of the ocean floor\n".to_string(),
            FtpStatus::FileStatus => "213 File status, examining a clam's shell for dinner\n".to_string(),
            FtpStatus::HelpMessage => "214 Help message, seeking guidance from a wise octopus\n".to_string(),
            FtpStatus::NameSystemType => format!("215 {} system type, identifying the underwater kingdom\n", env::consts::OS).to_string(),
            FtpStatus::ServiceReady => "220 Service ready for a new user, welcoming a stranger to the reef\n".to_string(),
            FtpStatus::ServiceClosingControlConnection => "221 Service closing control connection, saying goodbye to a friend on the shore\n".to_string(),
            FtpStatus::DataConnectionOpen => "225 Data connection open, opening its claws to catch dinner\n".to_string(),
            FtpStatus::ClosingDataConnection => "226 Closing data connection, savoring the last bites of a meal\n".to_string(),
            FtpStatus::EnteringPassiveMode(address) => format!("227 Entering Passive Mode {address}\n"),
            FtpStatus::EnteringLongPassiveMode => "228 Entering long passive mode, taking refuge in a deep sea trench\n".to_string(),
            FtpStatus::EnteringExtendedPassiveMode => "229 Entering extended passive mode, seeking safety in a complex underwater network of tunnels\n".to_string(),
            FtpStatus::UserLoggedIn => "230 User logged in, finding a peaceful nook to rest\n".to_string(),
            FtpStatus::UserAlreadyLoggedIn => "230 User already logged in, finding a peaceful nook to rest\n".to_string(),
            FtpStatus::UserLoggedOut => "231 User logged out, venturing out to hunt for food\n".to_string(),
            FtpStatus::LoggingOut => "232 Logging out, saying farewell to its underwater home\n".to_string(),
            FtpStatus::SecurityDataExchangeComplete => "234 Security data exchange complete, exchanging secret signals with fellow lobsters to stay protected\n".to_string(),
            FtpStatus::SecurityDataExchangeSuccessful => "235 Security data exchange successful, feeling secure in the lobster cluster\n".to_string(),
            FtpStatus::RequestedFileActionOk => "250 Requested file action okay, finding the perfect spot to crack open a clam\n".to_string(),
            FtpStatus::PathnameCreated(name, message) => format!("257 \"{name}\" {message}, building a new home in a new part of the ocean.\n"),
            FtpStatus::UserNameOkay => "331 User name okay, giving a friendly wave to a passing school of fish\n".to_string(),
            FtpStatus::NeedAccountForLogin => "332 Need account for login, asking a friendly starfish for directions to the bank\n".to_string(),
            FtpStatus::SecurityMechanismIsAccepted => "334 Security mechanism is accepted, sealing a deal with a giant clam for treasure\n".to_string(),
            FtpStatus::RequestedFileActionPendingFurtherInformation => "350 Requested file action pending further information, waiting for a sign from a wise sea turtle.\n".to_string(),
            FtpStatus::ServiceNotAvailable => "421 Service not available, the tide is out and the reef is exposed\n".to_string(),
            FtpStatus::CantOpenDataConnection => "425 Can't open data connection, the water is too murky to see\n".to_string(),
            FtpStatus::ConnectionClosed => "426 Connection closed, the tide is coming in and the reef is flooding\n".to_string(),
            FtpStatus::InvalidUserNameOrPassword => "430 Invalid username or password, a crab is trying to use a lobster's shell\n".to_string(),
            FtpStatus::RequestedHostUnavailable => "434 Requested host unavailable, a whale is blocking the way\n".to_string(),
            FtpStatus::RequestedFileActionNotTaken => "450 Requested file action not taken, a shark is circling\n".to_string(),
            FtpStatus::RequestedActionAborted => "451 Requested action aborted, chased away by a school of angry fish\n".to_string(),
            FtpStatus::RequestedActionNotTaken => "452 Requested action not taken, the tide is too low to reach the treasure\n".to_string(),
            FtpStatus::SyntaxErrorInParametersOrArguments => "501 Syntax error in parameters or arguments, mistaken by a fish for a rock\n".to_string(),
            FtpStatus::CommandNotImplementedUnknown => "502 Command not implemented, a crab's tool proving too small for the job\n".to_string(),
            FtpStatus::BadCommandSequence(help) => format!("503 Bad command sequence, {:?}; confused by a miscommunication with a fellow lobster\n", help),
            FtpStatus::CommandNotImplementedForThatParameter => "504 Command not implemented for that parameter, a clam's shell proving too small for the job\n".to_string(),
            FtpStatus::NotLoggedIn => "530 Not logged in, reveal yourself to other lobsters\n".to_string(),
            FtpStatus::NeedAccountForStoringFiles => "532 Need account for storing files, a starfish is asking for a loan\n".to_string(),
            FtpStatus::RequestDeniedForPolicyReasons => "533 Request denied for policy reasons, a whale is blocking the way\n".to_string(),
            FtpStatus::RequestedActionNotTakenFileUnavailable => "550 Requested action not taken, file unavailable, this pearl is out of reach\n".to_string(),
            FtpStatus::RequestedActionAbortedPageTypeUnknown => "551 Requested action aborted, page type unknown, a jellyfish might be blocking the way\n".to_string(),
            FtpStatus::RequestedFileActionAbortedExceededStorageAllocation => "552 Requested file action aborted, exceeded storage allocation, the treasure chest is full\n".to_string(),
            FtpStatus::RequestedActionNotTakenFileBusy => "553 Requested action not taken, file busy, a crab is trying to crack this pearl\n".to_string(),
            FtpStatus::IntegrityProtectedReply => "631 Connection abandoned, giving up the chase for prey\n".to_string(),
            FtpStatus::ConfidentialityAndIntegrityProtectedReply => "632 Confidentiality and integrity protected reply, safeguarded by a protective octopus\n".to_string(),
            FtpStatus::ConfidentialityProtectedReply => "633 Confidentiality protected reply, shielded by a mysterious sea creature\n".to_string(),
            FtpStatus::ConnectionResetByPeer => "10054 Connection reset by peer. 10054 Connection reset by peer, forcibly closed by remote host, maybe a strong wave took them.\n".to_string(),
            FtpStatus::CannotConnectToRemoteServer => "10060 Unable to connect with remote server, communication lost in a storm\n".to_string(),
            FtpStatus::CannotConnectToRemoteServerRefused => "10061 Remote server refusing connection, guarded by a powerful sea creature\n".to_string(),
            FtpStatus::NoRouteToHost => "10065 No route to host, lost in the vast ocean with no compass\n".to_string(),
            FtpStatus::DirectoryNotEmpty => "10066 Directory not empty, claimed by a hoard of treasure seeking crabs\n".to_string(),
            FtpStatus::TooManyUsers => "10068 Too many creatures in the ocean, server full, swarmed by a school of fish\n".to_string()
        }
    }
}
